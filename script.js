//GET all todos
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((data) => {
	let list = data.map((todo)=>{
		return todo.title;
	})
	console.log(list)
})


//GET a specific to do list
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then((data) => console.log(`The item "${data.title}" has a status of ${data.completed}`))

//POST
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));


//PUT
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PUT',
	headers:{
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
})

.then((response)=> response.json())
.then((data)=> console.log(data));

//PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PATCH',
	headers:{
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
	  	dateCompleted: '01/19/22'
	})
})

.then((response)=> response.json())
.then((data)=> console.log(data));

//DELETE
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE',
})
.then((response) => response.json())
.then((json) => console.log(json));